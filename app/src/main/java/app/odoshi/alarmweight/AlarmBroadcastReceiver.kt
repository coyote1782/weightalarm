package app.odoshi.alarmweight

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.view.View

class AlarmBroadcastReceiver: BroadcastReceiver()
{
    override fun onReceive(context: Context, intent: Intent)
    {
        val intentStartMainActivity: Intent? = Intent(context, WeightInputActivity::class.java)

        intentStartMainActivity?.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intentStartMainActivity)
    }
}