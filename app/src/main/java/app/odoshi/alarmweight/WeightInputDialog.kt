package app.odoshi.alarmweight

import android.app.AlertDialog
import android.app.Dialog
import android.support.v4.app.DialogFragment
import android.os.Bundle
import android.text.InputType
import android.widget.EditText
import android.widget.Toast

class WeightInputDialog: DialogFragment()
{
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog
    {
        val myedit = EditText(activity)
        myedit.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
        val builder = AlertDialog.Builder(activity).apply {

            setTitle("体重を入力してください")
            setView(myedit)
            setPositiveButton("OK") { _, _ ->
                // OKボタン押したときの処理
                val userText = myedit.getText().toString()
                val value = userText.toFloat()
                val myApplication = activity?.application as MyApplication

                myApplication.recordTodayWeight(value)

                val mainActivity = activity as MainActivity
                mainActivity.mainFragment?.updateWeight()
            }
            setNegativeButton("キャンセル", null)
        }

        return builder.create()
    }

}