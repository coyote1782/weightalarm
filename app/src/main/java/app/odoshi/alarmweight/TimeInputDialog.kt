package app.odoshi.alarmweight

import android.app.TimePickerDialog
import android.content.Context
import android.widget.TimePicker



class TimeInputDialog(context: Context?, themeResId: Int, listener: OnTimeSetListener?, hourOfDay: Int, minute: Int, is24HourView: Boolean)
    : TimePickerDialog(context, themeResId, listener, hourOfDay, minute, is24HourView)
{
    override fun onTimeChanged(view: TimePicker, hourOfDay: Int, minute: Int)
    {

    }
}