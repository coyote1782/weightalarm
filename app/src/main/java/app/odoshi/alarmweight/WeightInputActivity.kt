package app.odoshi.alarmweight

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.util.*

class WeightInputActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weight_input)

        val toolbar: Toolbar = findViewById(R.id.toolbar_weightInput)
        toolbar.setTitleTextColor(Color.WHITE)
        setSupportActionBar(toolbar)

        val timeText = findViewById<TextView>(R.id.textView_weightInputTime)
        val dateText = findViewById<TextView>(R.id.textView_weightInputDate)

        val myApp = application as MyApplication

        val userData = myApp.getUserData()
        userData?.let {
            val calendarNow = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)

            timeText.text = getString(R.string.timeFormat2, it.alarmHour, it.alarmMinutes)
            dateText.text = getString(R.string.dayFormat2,
                    calendarNow.get(Calendar.YEAR),
                    calendarNow.get(Calendar.MONTH) + 1,
                    calendarNow.get(Calendar.DATE))
        }

        val buttonOk: Button = findViewById(R.id.button_OK)
        val buttonMuri: Button = findViewById(R.id.button_muri)

        buttonOk.setOnClickListener {
            val editText: EditText? = findViewById(R.id.editText_weightInput)
            editText?.let {
                val userText = it.getText().toString()
                val value = userText.toFloat()
                // バリデーション
                myApp.recordTodayWeight(value)

                val intentStartMainActivity: Intent? = Intent(applicationContext, MainActivity::class.java)
                applicationContext.startActivity(intentStartMainActivity)
                finish()
            }
        }


    }


}
