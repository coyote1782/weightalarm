package app.odoshi.alarmweight

import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import io.realm.annotations.Required
import java.util.*

@RealmClass
open class UserData(
        @PrimaryKey
        open var id: String = UUID.randomUUID().toString(),

        /**
         * 初期化済みか
         */
        open var isInit : Boolean = false,
        /**
         * アラーム機能はOnか
         */
        open var isAlarmOn : Boolean = true,
        /**
         * 継続日数
         */
        open var runningCount : Int = 0,
        /**
         * アラーム時間
         */
//        open var alarmTime : Calendar? = null
        open var alarmHour : Int = 0,
        open var alarmMinutes : Int = 0
) : RealmObject()

@RealmClass
open class DayData(
        @PrimaryKey
        open var id: Int = 0,

        @Index
        open var year: Int = 0,
        @Index
        open var month: Int = 0,
        @Index
        open var day: Int = 0,

        /**
         * 記録済みか
         */
        open var isRecord: Boolean = false,
        /**
         * 本日の体重
         */
        open var todayWeight: Float = 0.0f
) : RealmObject()