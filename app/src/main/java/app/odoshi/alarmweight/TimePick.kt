package app.odoshi.alarmweight

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.widget.TimePicker
import java.util.*
import android.os.Build

class TimePick : DialogFragment()
{
    private var hour: Int = 0
    private var minute: Int = 0

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog
    {
        val bundle = arguments
        val isInit = bundle?.getBoolean("isInit")
        isInit?.let {
            if(!it)
            {
                val mainActivity = (context as MainActivity)
                val myapp = mainActivity.application as MyApplication
                val userData = myapp.getUserData()
                if(userData != null)
                {
                    hour = userData.alarmHour
                    minute = userData.alarmMinutes
                }
            }
            else
            {
                // to initialize a Calender instance
                val c = Calendar.getInstance()

                // at the first, to get the system current hour and minute
                hour = c.get(Calendar.HOUR_OF_DAY)
                minute = c.get(Calendar.MINUTE)
            }
        }

        val picker = TimePicker(activity)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            picker.hour = hour
            picker.minute = minute
        }
        picker.setOnTimeChangedListener { _, hourOfDay, minute ->
            this.hour = hourOfDay
            this.minute = minute
        }

        val builder = AlertDialog.Builder(activity)
        builder.setMessage(R.string.dialogTitle)
                .setPositiveButton(R.string.OK) {
                    _, _ -> (activity as MainActivity).onTimeSet(picker, hour, minute, true)
                }

        isInit?.let {
            if(!it)
            {
                builder.setNegativeButton(R.string.cancel) {
                    _, _ -> // nop
                }
            }
        }

        builder.setView(picker)
        return builder.create()
    }

}