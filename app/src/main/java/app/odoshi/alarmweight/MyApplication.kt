package app.odoshi.alarmweight

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.Toast
import io.realm.Realm
import io.realm.RealmConfiguration
import java.util.*

public class MyApplication: Application()
{
    private var instance: MyApplication? = null

    lateinit var realm : Realm

    fun getUserData() : UserData?
    {
        return realm.where(UserData::class.java).findFirst()
    }

    /**
     * 特定の日付のデータを取得
     */
    fun getDayData(year: Int, month: Int, day: Int) : DayData?
    {
        val id = year * 10000 + month * 100 + day
        return realm.where(DayData::class.java)
                .equalTo("id", id)
                .findFirst()
    }

    fun findOrCreateUserData(): UserData?
    {
        var userData = realm.where(UserData::class.java).findFirst()
        if(userData == null)
        {
            realm.executeTransaction{
                val userObj = realm.createObject(UserData::class.java, UUID.randomUUID().toString())
                realm.copyToRealm(userObj)
            }
            userData = realm.where(UserData::class.java).findFirst()
        }

        return userData
    }

    /**
     * 本日の体重を入力
     */
    fun recordTodayWeight(weight: Float)
    {
        val calendarNow = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)
        val id = calendarNow.get(Calendar.YEAR) * 10000 + calendarNow.get(Calendar.MONTH) * 100 + calendarNow.get(Calendar.DATE)
        var todayData = realm.where(DayData::class.java)
                .equalTo("id", id)
                .findFirst()

        if(todayData == null)
        {
            realm.executeTransaction {
                val todayObj = realm.createObject(DayData::class.java, id)
                realm.copyToRealm(todayObj)

                todayData = realm.where(DayData::class.java)
                        .equalTo("id", id)
                        .findFirst()
                todayData?.todayWeight = weight
            }

        }
        else
        {
            realm.executeTransaction {
                todayData?.todayWeight = weight
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        Realm.init(this)
        val realmConfig = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
        /**
         * 初期化の際はコメントアウトを外す
         */
//      Realm.deleteRealm(realmConfig)
        realm = Realm.getInstance(realmConfig)
    }

    fun onChangeSwitch(isAlarmOn: Boolean)
    {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(applicationContext, AlarmBroadcastReceiver::class.java)
        val pending = PendingIntent.getBroadcast(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        if(isAlarmOn)
        {
            val userData = realm.where(UserData::class.java).findFirst()
            if(null != userData)
            {
//                val calendarNow = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)
//                calendarNow.set(Calendar.HOUR_OF_DAY, userData.alarmHour)
//                calendarNow.set(Calendar.MINUTE, userData.alarmMinutes)
//                calendarNow.set(Calendar.SECOND, 0)

                val calendar = Calendar.getInstance() // Calendar取得
                calendar.timeInMillis = System.currentTimeMillis() // 現在時刻を取得
                calendar.add(Calendar.SECOND, 5) // 現時刻より15秒後を設定

                val intent3 = Intent(applicationContext, MainActivity::class.java)
                val pending3 = PendingIntent.getActivity(applicationContext, 0, intent3, 0)

                val alarmClockInfo = AlarmManager.AlarmClockInfo(calendar.timeInMillis, pending3)
                Toast.makeText(applicationContext, "setCalendar%d, %d, %d".format(calendar.get(Calendar.DATE), userData.alarmHour, userData.alarmMinutes), Toast.LENGTH_SHORT).show()
                alarmManager.setAlarmClock(alarmClockInfo, pending)
            }
        }
        else
        {

        }

        realm.executeTransaction {
            val userData = realm.where(UserData::class.java).findFirst()
            userData?.isAlarmOn = isAlarmOn
        }

        Toast.makeText(applicationContext, if(isAlarmOn) "Alarm on" else "Alarm off", Toast.LENGTH_SHORT).show()
    }

    override fun onTerminate() {
        super.onTerminate()

        realm.close()
    }

    fun getInstance(): MyApplication? {
        return instance
    }
}