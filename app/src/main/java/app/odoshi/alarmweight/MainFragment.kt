package app.odoshi.alarmweight


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.*
import android.widget.ProgressBar
import android.widget.Switch
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class MainFragment : Fragment()
{

    private lateinit var mTextView: TextView
    private lateinit var maker: String
    private lateinit var brand: String

    private lateinit var viewCache: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewCache = view

        val mainActivity = (context as MainActivity)
        val myApplication = activity?.application as MyApplication

        val userData = myApplication.getUserData()?: UserData()

        if(!userData.isInit)
        {
            mainActivity.showTimePickerDialog(viewCache, true)
        }
        else
        {
            createView()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun createView()
    {
        val view = viewCache

        val mainActivity = (context as MainActivity)
        val myApplication = activity?.application as MyApplication
        val userData = myApplication.getUserData()?: UserData()

        val switchView = view.findViewById<Switch>(R.id.alarmSwitch)
        switchView.isChecked = userData.isAlarmOn
        switchView.setOnCheckedChangeListener { buttonView, isChecked -> myApplication.onChangeSwitch(isChecked) }
        val runningDaysView = view.findViewById<TextView>(R.id.runningDays)
        runningDaysView.text = userData.runningCount.toString().plus(getString(R.string.day))

        val alarmTimeView = view.findViewById<TextView>(R.id.AlarmTime)
        val remainingTimeView = view.findViewById<TextView>(R.id.RemainingTime)
        val df = SimpleDateFormat(getString(R.string.timeFormat))
        val floatingActionButton2 = view.findViewById<FloatingActionButton>(R.id.floatingActionButton2)

        floatingActionButton2.setOnClickListener { v -> mainActivity.showTimePickerDialog(v, false) }

        val calendarAlarm = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)
        calendarAlarm.set(Calendar.HOUR_OF_DAY, userData.alarmHour)
        calendarAlarm.set(Calendar.MINUTE, userData.alarmMinutes)

        alarmTimeView.text = df.format(calendarAlarm.time)
        val calendarNow = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)

        val nowSeconds = calendarNow.get(Calendar.HOUR_OF_DAY) * 60 * 60
        + calendarNow.get(Calendar.MINUTE) * 60
        + calendarNow.get(Calendar.SECOND)
        val alarmSeconds = calendarAlarm.get(Calendar.HOUR_OF_DAY) * 60 * 60
        + calendarAlarm.get(Calendar.MINUTE) * 60
        + calendarAlarm.get(Calendar.SECOND)

        val isNextDay = alarmSeconds > nowSeconds
        calendarAlarm.set(Calendar.DATE, calendarNow.get(Calendar.DATE))
        if(!isNextDay)
        {
            calendarAlarm.add(Calendar.DATE, 1)
        }

        val diffMinutes = ((calendarAlarm.timeInMillis - calendarNow.timeInMillis)/ (1000 * 60)).toInt()
        remainingTimeView.text = String.format("%02d",
                (diffMinutes / 60))
                .plus(":")
                .plus(String.format("%02d", (diffMinutes % 60)).toString())

        val alarmCircleView = view.findViewById<ProgressBar>(R.id.AlarmCircle)
        val maxMinutes = 24 * 60

        val rate = diffMinutes.toFloat() / maxMinutes.toFloat()
        alarmCircleView.progress = (rate * 150.0f).toInt()

        val floatingActionButton = view.findViewById<FloatingActionButton>(R.id.floatingActionButton)
        floatingActionButton.setOnClickListener { v -> mainActivity.showWeightInputDialog(v) }

        updateWeight()
    }

    fun updateWeight()
    {
        val view = viewCache
        val mainActivity = (context as MainActivity)
        val calendarNow = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)

        val todayWeightView = view.findViewById<TextView>(R.id.todayWeight)

        val todayData = mainActivity.getDayData(calendarNow.get(Calendar.YEAR),
                calendarNow.get(Calendar.MONTH),
                calendarNow.get(Calendar.DATE))
        todayWeightView.text = "--"
        todayData?.let {
            todayWeightView.text = String.format(getString(R.string.kgFormat), it.todayWeight)
        }
    }

    companion object
    {
        // staticなやつ
    }

}
