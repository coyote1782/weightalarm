package app.odoshi.alarmweight

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TimePicker

import android.support.v7.widget.Toolbar
import android.graphics.Color
import android.view.Menu


class MainActivity : AppCompatActivity()
{
    var mainFragment: MainFragment? = null

    /**
     * 特定の日付のデータを取得
     */
    fun getDayData(year: Int, month: Int, day: Int) : DayData?
    {
        val id = year * 10000 + month * 100 + day
        val myapp = application as MyApplication
        return myapp.realm.where(DayData::class.java)
                .equalTo("id", id)
                .findFirst()
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null)
        {
            val toolbar: Toolbar = findViewById(R.id.toolbar)
            toolbar.setTitleTextColor(Color.WHITE)
            setSupportActionBar(toolbar)

            val transaction = supportFragmentManager.beginTransaction()
            mainFragment = MainFragment()
            val myApplication = application as MyApplication
            myApplication.findOrCreateUserData()
            transaction.add(R.id.activityMainLayout, mainFragment)
            transaction.commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    /**
     * @param isInit 一番最初に呼ばれるときのみtrue
     */
    fun showTimePickerDialog(view: View, isInit: Boolean)
    {
        val newFragment = TimePick()
        val bundle = Bundle()
        bundle.putBoolean("isInit", isInit)
        newFragment.arguments = bundle
        newFragment.show(supportFragmentManager, "timePicker")
    }

    fun showWeightInputDialog(view: View)
    {
        val newFragment = WeightInputDialog()
        newFragment.show(supportFragmentManager, "weightInput")
    }

    fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int, isOk : Boolean)
    {

        if(isOk) {
            val myApplication = application as MyApplication
            myApplication.realm.executeTransaction {
                val userData = myApplication.realm.where(UserData::class.java).findFirst()
                userData?.alarmHour = hourOfDay
                userData?.alarmMinutes = minute
                userData?.isInit = true
            }
        }

        mainFragment?.createView()
    }
}
